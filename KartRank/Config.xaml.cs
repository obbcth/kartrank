﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Newtonsoft.Json.Linq;

namespace KartRank
{
    /// <summary>
    /// Config.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Config : Window
    {
        Dictionary<string, int> ScoreBoard1 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard2 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard3 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard4 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard5 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard6 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard7 = new Dictionary<string, int>();
        Dictionary<string, int> ScoreBoard8 = new Dictionary<string, int>();

        Dictionary<string, string> MapBoard = new Dictionary<string, string>();

        public Config()
        {
            InitializeComponent();
            Execute();

            for (int i = 1; i <= 8; i++)
            {
                Grid NewGrid = new Grid();

                Button pl = new Button();
                pl.HorizontalAlignment = HorizontalAlignment.Left;
                pl.Margin = new Thickness(16);
                pl.Name = "Plus" + i.ToString();
                pl.Click += Plus_Click;

                Image ple = new Image();
                ple.Source = new BitmapImage(new Uri("plus.png", UriKind.Relative));
                ple.Height = 20;

                pl.Content = ple;

                Button mi = new Button();
                mi.HorizontalAlignment = HorizontalAlignment.Right;
                mi.Margin = new Thickness(16);
                mi.Name = "Minus" + i.ToString();
                mi.Click += Minus_Click;

                Image min = new Image();
                min.Source = new BitmapImage(new Uri("minus.png", UriKind.Relative));
                min.Height = 20;

                mi.Content = min;

                NewGrid.Children.Add(pl);
                NewGrid.Children.Add(mi);

                plma.Children.Add(NewGrid);
            }

            User1.Text = ((MainWindow)Application.Current.MainWindow).First.Text;
            User2.Text = ((MainWindow)Application.Current.MainWindow).Second.Text;
            User3.Text = ((MainWindow)Application.Current.MainWindow).Third.Text;
            User4.Text = ((MainWindow)Application.Current.MainWindow).Fourth.Text;
            User5.Text = ((MainWindow)Application.Current.MainWindow).Fifth.Text;
            User6.Text = ((MainWindow)Application.Current.MainWindow).Sixth.Text;
            User7.Text = ((MainWindow)Application.Current.MainWindow).Seventh.Text;
            User8.Text = ((MainWindow)Application.Current.MainWindow).Eighth.Text;
        }

        public int converter(int i)
        {
            switch (i)
            {
                case 1:
                    return 10;
                case 2:
                    return 7;
                case 3:
                    return 5;
                case 4:
                    return 4;
                case 5:
                    return 3;
                case 6:
                    return 1;
                case 7:
                    return 0;
                case 8:
                    return -1;
                default:
                    return -5;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch ((e.Source as RadioButton).GroupName)
            {
                case "1":
                    ScoreBoard1[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "2":
                    ScoreBoard2[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "3":
                    ScoreBoard3[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "4":
                    ScoreBoard4[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "5":
                    ScoreBoard5[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "6":
                    ScoreBoard6[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "7":
                    ScoreBoard7[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

                case "8":
                    ScoreBoard8[Page.Text] = Convert.ToInt32((e.Source as RadioButton).Content);
                    break;

            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToInt32(Page.Text) != 1)
            {
                Page.Text = (Convert.ToInt32(Page.Text) - 1).ToString();
                Execute();

                try
                {
                    MapName.Text = MapBoard[Page.Text] ;
                }
                catch {
                    MapName.Text = "(맵 이름)";
                }
            }
        }

        private void Front_Click(object sender, RoutedEventArgs e)
        {
            Page.Text = (Convert.ToInt32(Page.Text) + 1).ToString();
            Execute();

            try
            {
                MapName.Text = MapBoard[Page.Text];
            }
            catch
            {
                MapName.Text = "(맵 이름)";
            }
        }

        public void Execute()
        {
            NeedToExecute.Children.Clear();
            foreach (int j in Enumerable.Range(1, 8))
            {
                Grid DynamicGrid = new Grid();
                for (int i = 0; i < 9; i++)
                    DynamicGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                foreach (int i in Enumerable.Range(1, 9))
                {
                    RadioButton rad = new RadioButton();

                    rad.Name = "r" + j.ToString() + "r" + i.ToString();
                    rad.Height = 56;
                    rad.Content = converter(i);
                    rad.Click += Button_Click;
                    rad.GroupName = j.ToString();
                    if (j == 1)
                        rad.Margin = new Thickness(0, 20, 0, 0);
                    Grid.SetColumn(rad, i - 1);

                    try
                    {
                        switch (j)
                        {
                            case 1:
                                if (converter(i) == ScoreBoard1[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 2:
                                if (converter(i) == ScoreBoard2[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 3:
                                if (converter(i) == ScoreBoard3[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 4:
                                if (converter(i) == ScoreBoard4[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 5:
                                if (converter(i) == ScoreBoard5[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 6:
                                if (converter(i) == ScoreBoard6[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 7:
                                if (converter(i) == ScoreBoard7[Page.Text])
                                    rad.IsChecked = true;
                                break;
                            case 8:
                                if (converter(i) == ScoreBoard8[Page.Text])
                                    rad.IsChecked = true;
                                break;
                        }
                    }
                    catch { }

                    DynamicGrid.Children.Add(rad);
                }

                NeedToExecute.Children.Add(DynamicGrid);
            }
        }


        private void Save_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).First.Text = User1.Text;
            ((MainWindow)Application.Current.MainWindow).Second.Text = User2.Text;
            ((MainWindow)Application.Current.MainWindow).Third.Text = User3.Text;
            ((MainWindow)Application.Current.MainWindow).Fourth.Text = User4.Text;
            ((MainWindow)Application.Current.MainWindow).Fifth.Text = User5.Text;
            ((MainWindow)Application.Current.MainWindow).Sixth.Text = User6.Text;
            ((MainWindow)Application.Current.MainWindow).Seventh.Text = User7.Text;
            ((MainWindow)Application.Current.MainWindow).Eighth.Text = User8.Text;

            int sum = 0;
            foreach (var i in ScoreBoard1)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).First_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard2)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Second_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard3)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Third_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard4)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Fourth_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard5)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Fifth_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard6)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Sixth_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard7)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Seventh_.Text = sum.ToString();

            sum = 0;
            foreach (var i in ScoreBoard8)
                sum += i.Value;
            ((MainWindow)Application.Current.MainWindow).Eighth_.Text = sum.ToString();

        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            switch ((e.Source as Button).Name)
            {
                case "Plus1":
                    ((MainWindow)Application.Current.MainWindow).First.FontSize += 1;
                    break;
                case "Plus2":
                    ((MainWindow)Application.Current.MainWindow).Second.FontSize += 1;
                    break;
                case "Plus3":
                    ((MainWindow)Application.Current.MainWindow).Third.FontSize += 1;
                    break;
                case "Plus4":
                    ((MainWindow)Application.Current.MainWindow).Fourth.FontSize += 1;
                    break;
                case "Plus5":
                    ((MainWindow)Application.Current.MainWindow).Fifth.FontSize += 1;
                    break;
                case "Plus6":
                    ((MainWindow)Application.Current.MainWindow).Sixth.FontSize += 1;
                    break;
                case "Plus7":
                    ((MainWindow)Application.Current.MainWindow).Seventh.FontSize += 1;
                    break;
                case "Plus8":
                    ((MainWindow)Application.Current.MainWindow).Eighth.FontSize += 1;
                    break;
            }
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            switch ((e.Source as Button).Name)
            {
                case "Minus1":
                    if (((MainWindow)Application.Current.MainWindow).First.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).First.FontSize -= 1;
                    break;
                case "Minus2":
                    if (((MainWindow)Application.Current.MainWindow).Second.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Second.FontSize -= 1;
                    break;
                case "Minus3":
                    if (((MainWindow)Application.Current.MainWindow).Third.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Third.FontSize -= 1;
                    break;
                case "Minus4":
                    if (((MainWindow)Application.Current.MainWindow).Fourth.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Fourth.FontSize -= 1;
                    break;
                case "Minus5":
                    if (((MainWindow)Application.Current.MainWindow).Fifth.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Fifth.FontSize -= 1;
                    break;
                case "Minus6":
                    if (((MainWindow)Application.Current.MainWindow).Sixth.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Sixth.FontSize -= 1;
                    break;
                case "Minus7":
                    if (((MainWindow)Application.Current.MainWindow).Seventh.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Seventh.FontSize -= 1;
                    break;
                case "Minus8":
                    if (((MainWindow)Application.Current.MainWindow).Eighth.FontSize > 1)
                        ((MainWindow)Application.Current.MainWindow).Eighth.FontSize -= 1;
                    break;
            }
        }

        private void AutoLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var json = GetJson(1, User1.Text)["matches"][0]["matches"][0];
                var detail = GetJson(3, json["matchId"].ToString());

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("track.json"));

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    string jsonFile = reader.ReadToEnd(); //Make string equal to full file
                    JArray data = JArray.Parse(jsonFile);

                    MapName.Text = "정보없음";

                    foreach(var i in data)
                    {
                        if (i["id"].ToString() == detail["trackId"].ToString())
                            MapName.Text = i["name"].ToString();

                    }

                    MapBoard[Page.Text] = MapName.Text;

                }

                foreach (JObject i in detail["players"])
                {
                    if (i["matchRank"].ToString() == "")
                    {
                        if (i["characterName"].ToString() == User1.Text)
                            ScoreBoard1[Page.Text] = -5;
                        if (i["characterName"].ToString() == User2.Text)
                            ScoreBoard2[Page.Text] = -5;
                        if (i["characterName"].ToString() == User3.Text)
                            ScoreBoard3[Page.Text] = -5;
                        if (i["characterName"].ToString() == User4.Text)
                            ScoreBoard4[Page.Text] = -5;
                        if (i["characterName"].ToString() == User5.Text)
                            ScoreBoard5[Page.Text] = -5;
                        if (i["characterName"].ToString() == User6.Text)
                            ScoreBoard6[Page.Text] = -5;
                        if (i["characterName"].ToString() == User7.Text)
                            ScoreBoard7[Page.Text] = -5;
                        if (i["characterName"].ToString() == User8.Text)
                            ScoreBoard8[Page.Text] = -5;
                    }
                    else
                    {
                        if (i["characterName"].ToString() == User1.Text)
                            ScoreBoard1[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User2.Text)
                            ScoreBoard2[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User3.Text)
                            ScoreBoard3[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User4.Text)
                            ScoreBoard4[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User5.Text)
                            ScoreBoard5[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User6.Text)
                            ScoreBoard6[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User7.Text)
                            ScoreBoard7[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                        if (i["characterName"].ToString() == User8.Text)
                            ScoreBoard8[Page.Text] = converter(Convert.ToInt32(i["matchRank"].ToString()));
                    }
                }

                Execute();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (FormatException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (WebException)
            {
                MessageBox.Show("정보를 불러오지 못했습니다.\n첫 번째 라이더명에 오타가 있는지 확인하세요.");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "\n\n오류가 발생하였습니다.");
            }
        }

        public JObject GetJson(int n, string i)
        {
            string url = string.Empty;

            if (n == 1)
            {
                url = "https://api.nexon.co.kr/kart/v1.0/users/nickname/" + i;
            }
            else if (n == 2)
            {
                url = "https://api.nexon.co.kr/kart/v1.0/users/" + i + "/matches";
            }
            else if (n == 3)
            {
                url = "https://api.nexon.co.kr/kart/v1.0/matches/" + i;
            }

            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTEwNzUzODA1OCIsImF1dGhfaWQiOiIyIiwidG9rZW5fdHlwZSI6IkFjY2Vzc1Rva2VuIiwic2VydmljZV9pZCI6IjQzMDAxMTM5MyIsIlgtQXBwLVJhdGUtTGltaXQiOiIyMDAwMDoxMCIsIm5iZiI6MTU4Mjk1NzA1NywiZXhwIjoxNjQ2MDI5MDU3LCJpYXQiOjE1ODI5NTcwNTd9.08aMXgD_hE_C6TUbX0cgfdG7oxRp72OQl0FsIJIy58s"); // 헤더 추가 방법

            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
                {
                    HttpStatusCode status = resp.StatusCode;

                    Stream respStream = resp.GetResponseStream();
                    using (StreamReader sr = new StreamReader(respStream))
                    {
                        responseText = sr.ReadToEnd();
                    }
                }

                var json = JObject.Parse(responseText);

                if (n == 1)
                    return GetJson(2, json["accessId"].ToString());
                else
                    return json;
            }
            catch
            {
                throw;
            }
        }

        private void UserLoad_Click(object sender, RoutedEventArgs e)
        {
            string s = string.Empty;
            try
            {
                var json = GetJson(1, User1.Text)["matches"][0]["matches"][0];
                var detail = GetJson(3, json["matchId"].ToString());

                if (!detail["channelName"].ToString().Contains("Team"))
                {
                    foreach (JObject i in detail["players"])
                    {
                        if (i["characterName"].ToString() == User1.Text)
                        {
                            s = i["characterName"].ToString() + "\n" + s;
                        }
                        else
                        {
                            s = s + i["characterName"].ToString() + "\n";
                        }
                    }

                    try
                    {
                        if (MessageBox.Show(s + "\n라이더명을 반영할까요?", "라이더명 불러오기", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            // 색 깔맞춤

                            User2.Text = string.Empty;
                            User3.Text = string.Empty;
                            User4.Text = string.Empty;
                            User5.Text = string.Empty;
                            User6.Text = string.Empty;
                            User7.Text = string.Empty;
                            User8.Text = string.Empty;

                            User2.Text = s.Split()[1];
                            User3.Text = s.Split()[2];
                            User4.Text = s.Split()[3];
                            User5.Text = s.Split()[4];
                            User6.Text = s.Split()[5];
                            User7.Text = s.Split()[6];
                            User8.Text = s.Split()[7];
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }

                }
                else
                {
                    MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
                }

            }
            catch (NullReferenceException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (FormatException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (WebException)
            {
                MessageBox.Show("정보를 불러오지 못했습니다.\n첫 번째 라이더명에 오타가 있는지 확인하세요.");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("마지막으로 플레이한 게임이 스피드전 개인전이 맞는지 확인하세요.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "\n\n오류가 발생하였습니다.");
            }
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Help h = new Help();
            h.Show();
        }

        private void CopyToClipboard_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(((MainWindow)Application.Current.MainWindow).First.Text + " : " + ((MainWindow)Application.Current.MainWindow).First_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Second.Text + " : " + ((MainWindow)Application.Current.MainWindow).Second_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Third.Text + " : " + ((MainWindow)Application.Current.MainWindow).Third_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Fourth.Text + " : " + ((MainWindow)Application.Current.MainWindow).Fourth_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Fifth.Text + " : " + ((MainWindow)Application.Current.MainWindow).Fifth_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Sixth.Text + " : " + ((MainWindow)Application.Current.MainWindow).Sixth_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Seventh.Text + " : " + ((MainWindow)Application.Current.MainWindow).Seventh_.Text + "\n");
            sb.Append(((MainWindow)Application.Current.MainWindow).Eighth.Text + " : " + ((MainWindow)Application.Current.MainWindow).Eighth_.Text + "\n");

            Clipboard.SetText(sb.ToString());
        }
    }
}
