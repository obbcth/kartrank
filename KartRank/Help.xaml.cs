﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace KartRank
{
    /// <summary>
    /// Help.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Help : Window
    {
        public Help()
        {
            InitializeComponent();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help6.png")
            {
                imag.Source = new BitmapImage(new Uri("help7.png", UriKind.Relative));
                nextButton.IsEnabled = false;
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help5.png")
            {
                imag.Source = new BitmapImage(new Uri("help6.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help4.png")
            {
                imag.Source = new BitmapImage(new Uri("help5.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help3.png")
            {
                imag.Source = new BitmapImage(new Uri("help4.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help2.png")
            {
                imag.Source = new BitmapImage(new Uri("help3.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help1.png")
            {
                imag.Source = new BitmapImage(new Uri("help2.png", UriKind.Relative));
                backButton.IsEnabled = true;
            }
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help2.png")
            {
                imag.Source = new BitmapImage(new Uri("help1.png", UriKind.Relative));
                backButton.IsEnabled = false;
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help3.png")
            {
                imag.Source = new BitmapImage(new Uri("help2.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help4.png")
            {
                imag.Source = new BitmapImage(new Uri("help3.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help5.png")
            {
                imag.Source = new BitmapImage(new Uri("help4.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help6.png")
            {
                imag.Source = new BitmapImage(new Uri("help5.png", UriKind.Relative));
            }
            if (imag.Source.ToString() == "pack://application:,,,/KartRank;component/help7.png")
            {
                imag.Source = new BitmapImage(new Uri("help6.png", UriKind.Relative));
                nextButton.IsEnabled = true;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
        }
    }
}
