import requests
import json

headers = {'Authorization' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTEwNzUzODA1OCIsImF1dGhfaWQiOiIyIiwidG9rZW5fdHlwZSI6IkFjY2Vzc1Rva2VuIiwic2VydmljZV9pZCI6IjQzMDAxMTM5MyIsIlgtQXBwLVJhdGUtTGltaXQiOiIyMDAwMDoxMCIsIm5iZiI6MTU4Mjk1NzA1NywiZXhwIjoxNjQ2MDI5MDU3LCJpYXQiOjE1ODI5NTcwNTd9.08aMXgD_hE_C6TUbX0cgfdG7oxRp72OQl0FsIJIy58s'}
# developers.nexon.com 사이트에서 로그인 후 API 키 발급

n = input("닉네임 입력 : ")
# 라이더명으로 accessId 조회

url = "https://api.nexon.co.kr/kart/v1.0/users/nickname/" + n
response = requests.get(url, headers = headers)
d = json.loads(response.text)
print("액세스 아이디 : " + d["accessId"])

url2 = "https://api.nexon.co.kr/kart/v1.0/users/" + d["accessId"] + "/matches"
# accessId로 라이더의 최근 매치 전적 조회
# 기본값 10회로 예상

response2 = requests.get(url2, headers = headers)
d2 = json.loads(response2.text)

print(d2)

input()

# nickName : StreamLabs
# matches
## matchType : 7b9f0fd5377c38514dbb78ebe63ac6c3b81009d5a31dd569d1cff8f005aa881a (스피드전)
## matches
### matchId : 00650010d11a45c1 (플레이어 8명 비교 대조하기)
### trackId : f07ff223032e20e10ec4ca3a36a34cdefc55f7b7cfdb17adebc0f5bffd26f883 (metadata에서 조회)
### playerCount : 8
### player
#### matchRetired : 0 / 1
#### matchRank : 5 (5등)